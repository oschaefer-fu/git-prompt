# lewein.sh
# (c) Oliver schäfer, 15.08.2017
#
# Die Datei gehört unter Scientific Linux in das Verzeichnis /etc/profile.d.
# Dort liegende *.sh-Dateien werden beim Start jeder non-login-Shell ausge-
# führt. Setzt Farben und Prompt in der Konsole und zeigt bei Vorhandensein
# eines git-Branches den Namen des aktuellen Branches an.

# ############################ set darkcolors ################################
# setzt die Farben für das Terminal mit dunklem Hintergrund
function set_darkcolors () {
(
cat <<EOFdark
# Configuration file for dircolors, a utility to help you set the
# LS_COLORS environment variable used by GNU ls with the --color option.
# Copyright (C) 1996, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006
# Free Software Foundation, Inc.
# Copying and distribution of this file, with or without modification,
# are permitted provided the copyright notice and this notice are preserved.
# The keywords COLOR, OPTIONS, and EIGHTBIT (honored by the
# slackware version of dircolors) are recognized but ignored.
# Below, there should be one TERM entry for each termtype that is colorizable
TERM Eterm
TERM ansi
TERM color-xterm
TERM con132x25
TERM con132x30
TERM con132x43
TERM con132x60
TERM con80x25
TERM con80x28
TERM con80x30
TERM con80x43
TERM con80x50
TERM con80x60
TERM cons25
TERM console
TERM cygwin
TERM dtterm
TERM gnome
TERM konsole
TERM kterm
TERM linux
TERM linux-c
TERM mach-color
TERM mlterm
TERM putty
TERM rxvt
TERM rxvt-cygwin
TERM rxvt-cygwin-native
TERM rxvt-unicode
TERM screen
TERM screen-bce
TERM screen-w
TERM screen.linux
TERM vt100
TERM xterm
TERM xterm-88color
TERM xterm-256color
TERM xterm-color
TERM xterm-debian
# Below are the color init strings for the basic file types. A color init
# string consists of one or more of the following numeric codes:
# Attribute codes:
# 00=none 01=bold 04=underscore 05=blink 07=reverse 08=concealed
# Text color codes:
# 30=black 31=red 32=green 33=yellow 34=blue 35=magenta 36=cyan 37=white
# Background color codes:
# 40=black 41=red 42=green 43=yellow 44=blue 45=magenta 46=cyan 47=white
NORMAL 00 # global default, although everything should be something.
FILE 00 # normal file
DIR 04;31 # directory
LINK 01;36 # symbolic link. (If you set this to 'target' instead of a
 # numerical value, the color is as for the file pointed to.)
FIFO 40;33 # pipe
SOCK 01;35 # socket
DOOR 01;35 # door
BLK 40;33;01 # block device driver
CHR 40;33;01 # character device driver
ORPHAN 40;31;01 # symlink to nonexistent file, or non-stat'able file
SETUID 37;41 # file that is setuid (u+s)
SETGID 30;43 # file that is setgid (g+s)
STICKY_OTHER_WRITABLE 30;42 # dir that is sticky and other-writable (+t,o+w)
OTHER_WRITABLE 34;42 # dir that is other-writable (o+w) and not sticky
STICKY 37;44 # dir with the sticky bit set (+t) and not other-writable
# This is for files with execute permission:
EXEC 01;32
# List any file extensions like '.gz' or '.tar' that you would like ls
# to colorize below. Put the extension, a space, and the color init string.
# (and any comments you want to add after a '#')
# If you use DOS-style suffixes, you may want to uncomment the following:
#.cmd 01;32 # executables (bright green)
#.exe 01;32
#.com 01;32
#.btm 01;32
#.bat 01;32
# Or if you want to colorize scripts even if they do not have the
# executable bit actually set.
#.sh 01;32
#.csh 01;32
 # archives or compressed (bright red)
.tar 01;31
.tgz 01;31
.arj 01;31
.taz 01;31
.lzh 01;31
.zip 01;31
.z 01;31
.Z 01;31
.gz 01;31
.bz2 01;31
.bz 01;31
.tbz2 01;31
.tz 01;31
.deb 01;31
.rpm 01;31
.jar 01;31
.rar 01;31
.ace 01;31
.zoo 01;31
.cpio 01;31
.7z 01;31
.rz 01;31
# image formats
.jpg 01;35
.JPG 01;35
.jpeg 01;35
.JPEG 01;35
.gif 01;35
.bmp 01;35
.pbm 01;35
.pgm 01;35
.ppm 01;35
.tga 01;35
.xbm 01;35
.xpm 01;35
.tif 01;35
.tiff 01;35
.png 01;35
.mng 01;35
.pcx 01;35
.mov 01;35
.mpg 01;35
.mpeg 01;35
.m2v 01;35
.mkv 01;35
.ogm 01;35
.mp4 01;35
.m4v 01;35
.mp4v 01;35
.vob 01;35
.qt 01;35
.nuv 01;35
.wmv 01;35
.asf 01;35
.rm 01;35
.rmvb 01;35
.flc 01;35
.avi 01;35
.fli 01;35
.gl 01;35
.dl 01;35
.xcf 01;35
.xwd 01;35
.yuv 01;35
# audio formats
.aac 00;36
.au 00;36
.flac 00;36
.mid 00;36
.midi 00;36
.mka 00;36
.mp3 00;36
.mpc 00;36
.ogg 00;36
.ra 00;36
.wav 00;36
# programming languages, TeX
.def 00;33
.godef 00;33
.mod 00;32
.go 00;32
.moa 00;32
.mob 00;32
.moc 00;32
.moe 00;32
.mof 00;32
.m 00;32
.lit.m 00;32
.x 00;37
.h 00;33
.c 00;32
.o 00;37
.hs 00;32
.lit.hs 00;32
.py 00;32
.pl 00;32
.tex 01;34
.toc 00;37
.bbl 00;37
.aux 00;37
.idx 00;37
.log 00;37
.ps 01;34
.pdf 01;34
.dvi 01;34
EOFdark
) > /tmp/darkcolors
eval `dircolors /tmp/darkcolors`
rm -f /tmp/darkcolors
}

# ########################### set lightcolors ################################
# setzt die Farben für das Terminal mit hellem Hintergrund
function set_lightcolors () {
(
cat <<EOFlight
# Configuration file for dircolors, a utility to help you set the
# LS_COLORS environment variable used by GNU ls with the --color option.
# Copyright (C) 1996, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006
# Free Software Foundation, Inc.
# Copying and distribution of this file, with or without modification,
# are permitted provided the copyright notice and this notice are preserved.
# The keywords COLOR, OPTIONS, and EIGHTBIT (honored by the
# slackware version of dircolors) are recognized but ignored.
# Below, there should be one TERM entry for each termtype that is colorizable
TERM Eterm
TERM ansi
TERM color-xterm
TERM con132x25
TERM con132x30
TERM con132x43
TERM con132x60
TERM con80x25
TERM con80x28
TERM con80x30
TERM con80x43
TERM con80x50
TERM con80x60
TERM cons25
TERM console
TERM cygwin
TERM dtterm
TERM gnome
TERM konsole
TERM kterm
TERM linux
TERM linux-c
TERM mach-color
TERM mlterm
TERM putty
TERM rxvt
TERM rxvt-cygwin
TERM rxvt-cygwin-native
TERM rxvt-unicode
TERM screen
TERM screen-bce
TERM screen-w
TERM screen.linux
TERM vt100
TERM xterm
TERM xterm-256color
TERM xterm-88color
TERM xterm-color
TERM xterm-debian
# Below are the color init strings for the basic file types. A color init
# string consists of one or more of the following numeric codes:
# Attribute codes:
# 00=none 01=bold 04=underscore 05=blink 07=reverse 08=concealed
# Text color codes:
# 30=black 31=red 32=green 33=yellow 34=blue 35=magenta 36=cyan 37=white
# Background color codes:
# 40=black 41=red 42=green 43=yellow 44=blue 45=magenta 46=cyan 47=white
NORMAL 00 # global default, although everything should be something.
FILE 00 # normal file
DIR 04;34 # directory
LINK 01;36 # symbolic link. (If you set this to 'target' instead of a
 # numerical value, the color is as for the file pointed to.)
FIFO 40;33 # pipe
SOCK 01;35 # socket
DOOR 01;35 # door
BLK 40;33;01 # block device driver
CHR 40;33;01 # character device driver
ORPHAN 40;31;01 # symlink to nonexistent file, or non-stat'able file
SETUID 37;41 # file that is setuid (u+s)
SETGID 30;43 # file that is setgid (g+s)
STICKY_OTHER_WRITABLE 30;42 # dir that is sticky and other-writable (+t,o+w)
OTHER_WRITABLE 34;42 # dir that is other-writable (o+w) and not sticky
STICKY 37;44 # dir with the sticky bit set (+t) and not other-writable
# This is for files with execute permission:
EXEC 01;32
# List any file extensions like '.gz' or '.tar' that you would like ls
# to colorize below. Put the extension, a space, and the color init string.
# (and any comments you want to add after a '#')
# If you use DOS-style suffixes, you may want to uncomment the following:
#.cmd 01;32 # executables (bright green)
#.exe 01;32
#.com 01;32
#.btm 01;32
#.bat 01;32
# Or if you want to colorize scripts even if they do not have the
# executable bit actually set.
#.sh 01;32
#.csh 01;32
# archives or compressed (bright red)
.tar 01;31
.tgz 01;31
.arj 01;31
.taz 01;31
.lzh 01;31
.zip 01;31
.z 01;31
.Z 01;31
.gz 01;31
.bz2 01;31
.bz 01;31
.tbz2 01;31
.tz 01;31
.deb 01;31
.rpm 01;31
.jar 01;31
.rar 01;31
.ace 01;31
.zoo 01;31
.cpio 01;31
.7z 01;31
.rz 01;31
# image formats
.jpg 01;35
.jpeg 01;35
.gif 01;35
.bmp 01;35
.pbm 01;35
.pgm 01;35
.ppm 01;35
.tga 01;35
.xbm 01;35
.xpm 01;35
.tif 01;35
.tiff 01;35
.png 01;35
.mng 01;35
.pcx 01;35
.mov 01;35
.mpg 01;35
.mpeg 01;35
.m2v 01;35
.mkv 01;35
.ogm 01;35
.mp4 01;35
.m4v 01;35
.mp4v 01;35
.vob 01;35
.qt 01;35
.nuv 01;35
.wmv 01;35
.asf 01;35
.rm 01;35
.rmvb 01;35
.flc 01;35
.avi 01;35
.fli 01;35
.gl 01;35
.dl 01;35
.xcf 01;35
.xwd 01;35
.yuv 01;35
# audio formats
.aac 00;36
.au 00;36
.flac 00;36
.mid 00;36
.midi 00;36
.mka 00;36
.mp3 00;36
.mpc 00;36
.ogg 00;36
.ra 00;36
.wav 00;36
# programming languages, TeX
.def 00;33
.godef 00;33
.mod 00;32
.go 00;32
.moa 00;32
.mob 00;32
.moc 00;32
.moe 00;32
.mof 00;32
.m 00;32
.lit.m 00;32
.x 01;30
.h 00;33
.c 00;32
.o 01;30
.hs 00;32
.lit.hs 00;32
.py 00;32
.pl 00;32
.tex 01;34
.toc 01;30
.bbl 01;30
.aux 01;30
.idx 01;30
.log 01;30
.ps 01;34
.pdf 01;34
.dvi 01;34
EOFlight
) > /tmp/lightcolors
eval `dircolors /tmp/lightcolors`
rm -f /tmp/lightcolors
}

# ########################### set git-prompt #################################

source /etc/profile.d/git-prompt.sh

# ############################ Farben setzen #################################
export LC_ALL=de_DE.UTF8
export LANG=de_DE.UTF8
export LC_COLLATE=POSIX
export EDITOR=vim

umask 022

# Farben für diverse Dateiendungen festlegen
case $TERM in
  xterm*|konsole*|rxvt*) 
    if [ "$BG" = "dark" ]; then 
      set_darkcolors
    elif [ "$BG" = "light" ]; then
      set_lightcolors
    elif [ -z $BG ]; then
      export BG="dark"
      set_darkcolors
    fi ;;
  *)  set_darkcolors
      export BG="dark";;
esac

# ############################## setprompt ###################################
# set_prompt legt das Aussehen des 
# Referenzierte extern gesetzte Variablen: SSH_CONNECTION UID TERM

# (c) Peter Bartke (198x-2011)
# überarbeitet und erweitert von Oliver Schäfer, 15.08.2017

TTY=`tty`
if [ -n "$SSH_CONNECTION" ]; then # wir sind  auf einem anderen Rechner
REMOTE=1                     # mit ssh eingeloggt
HOSTCOLOR="\[\033[1;31m\]"   # -> hellrot
else                           # lokal
HOSTCOLOR="\[\033[0;32m\]"   # -> gruen
fi
if [ $UID = 0 ]; then          # wir sind root
PR='#'                       # Prompt #
USERCOLOR="\[\033[1;31m\]"   # Nutzer in hellrot  
else 
PR='$'                       # Prompt $
USERCOLOR="\[\033[0;32m\]"   # Nutzer in gruen 
fi
DIRCOLOR="\[\033[0;31m\]"      # Farbe des aktuellen Verzeichnisses: rot
CONCOLOR="\[\033[1;37m\]"      # Farbe der Konsolennummer: hellweiss
RCONCOLOR="\[\033[1;36m\]"     # Farbe der fernen Konsole: hellblau
if [ "$BG" = "dark" ]; then
BRANCHCOLOR="\[\033[1;33m\]"   # aktueller GIT-Branch: gelb
else
BRANCHCOLOR="\[\033[1;34m\]"   # aktueller GIT-Branch: blau
fi
CONSOLE="\[\033[m\]"           # Leeres Konsolenfeld
FINIS="\[\033[m\]"             # Leeres Konsolenfeld
case $TERM in                  # Abhängig vom Terminal setzen wir
xterm* ) TITLEBAR="\[\033]0;xterm \u@\h: \w\007\]"  
   if [ $REMOTE ]; then  # falls wir remote sind: die pts-Nummer
     CONSOLE=".${RCONCOLOR}${TTY##/dev/pts/}\[\033[0m\]" 
   fi
   # in lokalen xterms keine pts-Nummern 
   ;;
konsole*) TITLEBAR="\[\033]0;konsole \u@\h: \w\007\]" # bei kterms ebenso
   ;;
linux*)  TITLEBAR=""           # und sonst 
   if [ $REMOTE ]; then  # falls wir remote sind: die pts-Nummer
     CONSOLE=".${RCONCOLOR}${TTY##/dev/pts/}\[\033[0m\]" 
   else  # und lokal die tty-Nummer
     if [ -r /etc/slackware-version -o -r /etc/arch-release ]; then
       CONSOLE=".${CONCOLOR}${TTY##/dev/vc/}\[\033[0m\]" 
     else
       CONSOLE=".${CONCOLOR}${TTY##/dev/tty}\[\033[0m\]" 
     fi
   fi
   ;;
esac

if [ "$UID" == "0" ]; then
# Prompt, bunt, gilt für root (ohne git-Umgebungsvariablen)
PS1="${TITLEBAR}${USERCOLOR}\u\[\033[0m\]@${HOSTCOLOR}\h\[\033[0m\]${CONSOLE} ${DIRCOLOR}\w${FINIS}\n${PR} " 
else
# Prompt, inklusive aktueller Branch in git
PS1="${TITLEBAR}${USERCOLOR}\u\[\033[0m\]@${HOSTCOLOR}\h\[\033[0m\]${CONSOLE} ${DIRCOLOR}\w${FINIS}\n${BRANCHCOLOR}"\
'$(__git_ps1 "(%s) ")'${FINIS}${PR}" "
fi

export TTY PS1

# ################################## alias ####################################

# set_alias setzt benutzerspezifische Aliase. Dieser Bereich kann nach
# Belieben ausgebaut werden. Alternativ kann auch eine lokale .alias-Datei
# im Home-Verzeichnis gepflegt werden
#
# Oliver Schäfer, 15.08.2017

alias ls='ls --color'
alias ll='ls --color -lh'
alias llt='ls --color -ltr'
alias lld='ls --color -ld */'
alias lla='ls --color -alt'
alias llr='ls --color -lR'
alias vi=vim
alias vimx=vim
alias mira="rlwrap mira"
alias hugs="rlwrap hugs"
alias cd..='cd ..'
alias l2u='recode latin1..utf8'
alias u2l='recode utf8..latin1'
alias gmutt='/usr/bin/mutt -F ~/.mutt/muttrc.gmx'
alias dmutt='/usr/bin/mutt -F ~/.mutt/muttrc.dhs'
alias dlmutt='/usr/bin/mutt -F ~/.mutt/muttrc.dhg-local'
alias tmutt='/usr/bin/mutt -F ~/.mutt/muttrc.telekom'
alias lmutt='/usr/bin/mutt -F ~/.mutt/muttrc.local'
alias zmutt='/usr/bin/mutt -F ~/.mutt/muttrc.zedat'
alias mutt='echo "Bitte (d|g|l|t|z)mutt eingeben"'
#alias cdPath='. ~/bin/cdPath'
alias maxima='rlwrap maxima'
alias ij='rlwrap ij'
alias aribas='rlwrap ~/bin/aribas'
alias mycp='rsync -rth --progress'
alias jflap='java -jar ~/bin/JFLAP.jar'
alias mars='java -jar ~/bin/MARS.jar'
alias umoutn='umount'
alias python='echo "ACHTUNG: Aufgerufen wird der Python-3-Interpreter!"; echo; python3'
alias processing='cd ~/bin/processing-3.3.3; ./processing'

PATH=$PATH:.
export PATH

# setPath, clearPath
SETPATHFILE=/home/lewein/.setpath
alias setPath='echo "cd `pwd`" > $SETPATHFILE;echo "gesetzt auf `pwd`"'
alias clearPath='rm $SETPATHFILE;echo "gesetzt auf ~"'
alias cdPath='source $SETPATHFILE'
if [ -f /home/$USER/.setpath ]; then
  source /home/$USER/.setpath
fi
